#    Copyright (C) 2019  michael mccune
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import logging


class Backend():
    """The default backend

    This backend will only log debug messages about events on the datadir.
    """
    name = "Default"

    def __init__(self, configdir):
        self.configdir = configdir
        self.cookies = {}
        logging.debug('{} backend created'.format(self.name))

    def cleanup(self):
        """cleanup the backend

        this function will get called before the application exits.
        """
        logging.debug('{} backend cleanup'.format(self.name))

    def cookie_add(self, cookie, data):
        """add a cookie and filename to the registry"""
        self.cookies[cookie] = data
        logging.debug('Adding cookie {} ({})'.format(cookie, data))

    def cookie_remove(self, cookie):
        """remove the cookie from registry and return its data

        returns None if the cookie is absent.
        """
        retval = self.cookies.get(cookie)
        logging.debug('Removing cookie {} ({})'.format(cookie, retval))
        return retval

    def dispatch(self, event):
        """dispatch events to the handlers

        this function will get called for each incoming event, it will then
        call the individual event handling routines.
        """
        should_process = self.pre_dispatch(event)

        if should_process:
            logging.debug(
                    'dispatching event: PATH={} FILENAME={} EVENT_TYPES={}'.
                    format(event.path, event.filename, event.type_names))

            # i'm not sure how many of these type_names can occur in the same
            # event. i have taken a best guess at the processing order for
            # these event types, this could probably be improved with a deeper
            # understanding of the inotify interface.
            if 'IN_MOVED_FROM' in event.type_names:
                self.in_moved_from(event)

            if 'IN_CLOSE_WRITE' in event.type_names:
                self.in_close_write(event)

            if 'IN_DELETE' in event.type_names:
                self.in_delete(event)

            if 'IN_MOVED_TO' in event.type_names:
                self.in_moved_to(event)

        self.post_dispatch(event)

    def in_close_write(self, event):
        """IN_CLOSE_WRITE handler"""
        logging.debug(
                'IN_CLOSE_WRITE on {} {}'.format(event.path, event.filename))

    def in_delete(self, event):
        """IN_DELETE handler"""
        logging.debug(
                'IN_DELETE on {} {}'.format(event.path, event.filename))

    def in_moved_from(self, event):
        """IN_MOVED_FROM handler"""
        logging.debug(
                'IN_MOVED_FROM on {} {} cookie {}'.format(
                event.path, event.filename, event.header.cookie))

    def in_moved_to(self, event):
        """IN_MOVED_TO handler"""
        logging.debug(
                'IN_MOVED_TO on {} {} cookie {}'.format(
                event.path, event.filename, event.header.cookie))

    def initialize(self, datadir):
        """initialize the backend

        this function will get called after the class has been created but
        before any events have been dispatched. it should be used to perform
        any sort of backend specific initialization (eg pull files from a
        remote server).
        """
        self.datadir = datadir
        logging.debug('{} backend initialized'.format(self.name))

    def pre_dispatch(self, event):
        """pre event handler

        this is called before each event is processed, it returns True
        if event processing should continue and False otherwise.
        """
        logging.debug(
                'pre dispatching event: PATH={} FILENAME={} EVENT_TYPES={}'.
                format(event.path, event.filename, event.type_names))
        return True

    def post_dispatch(self, event):
        """post event handler

        this is called after each event has been dispatched.
        """
        logging.debug(
                'post dispatching event: PATH={} FILENAME={} EVENT_TYPES={}'.
                format(event.path, event.filename, event.type_names))


