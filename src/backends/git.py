#    Copyright (C) 2019  michael mccune
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import logging
import pathlib
import subprocess
import time

from .base import Backend


class Git(Backend):
    """A backend for Git repositories

    This backend requires a Secret with roughly the following schema:
    ---
    kind: Secret
    apiVersion: v1
    metadata:
      name: my-secret
    stringData:
      backend: git
      git.url: <some ssh@ or git@ repository url>
      git.ref: <the branch or reference to pull, default: master>
      git.key: |+
        <an ssh private key for Git>
    """
    name = "Git"

    @staticmethod
    def run(command, ex_msg, git_dir=None):
        if git_dir:
            cli = 'git -C {} {}'.format(git_dir, command)
        else:
            cli = 'git {}'.format(command)

        cmd = subprocess.run(cli, shell=True)
        if cmd.returncode != 0:
            raise Exception(ex_msg)
        return cmd

    def initialize(self, datadir):
        logging.debug('Initializing Git backend')

        self.branch = "master"
        self.datadir = datadir
        self._moved_from = None
        keyfile = pathlib.Path('/etc/mirrorshade/git.key')
        if keyfile.exists() is False:
            raise Exception('No git.key file found')
        cmd = Git.run('config --global core.sshCommand '
                      '"ssh -o StrictHostKeyChecking=no -i {}"'
                      .format(keyfile), 'Unable to set core.sshCommand')
        logging.debug('Set core.sshCommand')
        cmd = Git.run('config --global user.name mirrorshade-bot',
                      'Unable to set user.name')
        logging.debug('Set user.name')
        cmd = Git.run('config --global user.email mirrorshade-bot@localhost',
                      'Unable to set user.email')
        logging.debug('Set user.email')

        urlfile = pathlib.Path('{}/git.url'.format(self.configdir))
        if urlfile.exists() is False:
            raise Exception('No git.url file found')
        url = open(urlfile).read().strip()
        cmd = subprocess.run('git clone --depth 1 {} {}'.format(url, datadir),
                             shell=True)
        if cmd.returncode != 0:
            raise Exception('Unable to clone {}'.format(url))
        logging.debug('Cloned repository')
        self.lastupdate = time.time()

    def pre_dispatch(self, event):
        logging.debug('Git backend pre-dispatch')
        self.should_commit = False
        self.commit_msg = None
        self.should_push = False
        self.relfilename = '{}/{}'.format(
                event.path.replace(self.datadir, '', 1), event.filename)
        if self.relfilename.startswith('/'):
            self.relfilename = '.{}'.format(self.relfilename)

        if self.relfilename.startswith('./.git/'):
            return False

        return True

    def in_moved_from(self, event):
        # save the old file name so we can run the git move command
        self.cookie_add(event.header.cookie, self.relfilename)

    def in_close_write(self, event):
        logging.debug('Adding {}'.format(self.relfilename))
        try:
            cmd = Git.run(
                    'add {}'.format(self.relfilename),
                    'Unable to add {}'.format(self.relfilename),
                    self.datadir)
            self.should_commit = True
            self.commit_msg = "Update {} at {}".format(
                    self.relfilename, time.time())
        except Exception as ex:
            logging.debug(ex)

    def in_delete(self, event):
        logging.debug('Removing {}'.format(self.relfilename))
        try:
            cmd = Git.run(
                    'rm {}'.format(self.relfilename),
                    'Unable to remove {}'.format(self.relfilename),
                    self.datadir)
            # don't commit if we are moving a file
            if self.relfilename not in self.cookies.values():
                self.should_commit = True
            self.commit_msg = 'Remove {} at {}'.format(
                    self.relfilename, time.time())
        except Exception as ex:
            logging.debug(ex)

    def in_moved_to(self, event):
        logging.debug('Moving {}'.format(self.relfilename))
        try:
            old_name = self.cookie_remove(event.header.cookie)
            if old_name is not None:
                cmd = Git.run(
                        'rm {}'.format(old_name),
                        'Unable to remove {}'.format(old_name),
                        self.datadir)
                cmd = Git.run(
                        'add {}'.format(self.relfilename),
                        'Unable to move {}'.format(self.relfilename),
                        self.datadir)
                self.should_commit = True
                self.commit_msg = 'Moved {} at {}'.format(
                        self.relfilename, time.time())
        except Exception as ex:
            logging.debug(ex)

    def post_dispatch(self, event):
        logging.debug('Git backend post-dispatch')
        if self.should_commit:
            logging.debug('Committing Git repository')
            try:
                cmd = Git.run(
                        'commit -m "{}"'.format(self.commit_msg),
                        'Unable to commit {}'.format(self.relfilename),
                        self.datadir)

                now = time.time()
                if (now - self.lastupdate) > 3.0:
                    # if 3 seconds have elapsed, let's do a push
                    self.should_push = True
                    self.lastupdate = now
            except Exception as ex:
                # `git commit` will return 1 if there is nothing to commit
                logging.debug('Nothing to commit')

        if self.should_push:
            try:
                logging.debug('Pushing Git repository')
                cmd = Git.run(
                        'push origin {}'.format(self.branch),
                        'Unable to push',
                        self.datadir)
            except Exception as ex:
                logging.debug(ex)
