#    Copyright (C) 2019  michael mccune
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import logging
import pathlib

from .base import Backend
from .git import Git


def create(configdir):
    """create a new backend interface

    this will return a backend object based on the configuration file
    present. by default it will return the base `Backend` object.
    """
    backend = None
    backendcls = Backend
    backendfile = pathlib.Path('{}/backend'.format(configdir))
    logging.debug('Looking for backend config {}'.format(backendfile))
    if backendfile.exists():
        backend = open(backendfile).read().strip()
        logging.debug('Found backend config {}'.format(backend))
    if backend == 'git':
        backendcls = Git

    return backendcls(configdir)
