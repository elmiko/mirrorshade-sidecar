#    Copyright (C) 2019  michael mccune
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import atexit
import logging
import pathlib
import sys
import traceback

import inotify.adapters

import backends
import events


# Backends
##############################################################################
# Main functions
##############################################################################
def get_or_create_datadir():
    path = '/datadir'
    datadir = pathlib.Path(path)
    if datadir.exists() is False:
        datadir.mkdir()
        logging.debug('creating data directory {}'.format(path))
    if datadir.exists() and datadir.is_dir() is False:
        raise Exception('Path {} exists and is not a directory'.format(path))
    return path


def watch_and_dispatch(datadir, update_func):
    i = inotify.adapters.InotifyTree(datadir)
    logging.debug('beginning watch and update on {}'.format(datadir))
    for event in i.event_gen(yield_nones=False):
        update_func(events.InotifyEvent(event))


def main():
    logging.info('start mirrorshade')
    datadir = get_or_create_datadir()
    logging.info('data directory is {}'.format(datadir))
    configdir = '/etc/mirrorshade'
    backend = backends.create(configdir)
    logging.info('backend is {}'.format(backend.name))
    backend.initialize(datadir)
    atexit.register(backend.cleanup)
    watch_and_dispatch(datadir, backend.dispatch)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    try:
        main()
    except Exception as ex:
        traceback.print_exc()
        sys.exit(1)
