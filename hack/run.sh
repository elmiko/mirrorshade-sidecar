#!/bin/sh
# this script is an exampe of how to run mirroshade-sidecar in a local container.
#
# in this example `$HOME/tmp/datadir` is mounted as the common data directory that
# mirroshade will watch, and `$HOME/tmp/mirrorshade` is the directory containing
# configuration files for mirrorshade.
#
podman run --rm -it -v $HOME/tmp/datadir:/datadir:Z -v $HOME/tmp/mirrorshade:/etc/mirrorshade:Z localhost/mirrorshade-sidecar $1
