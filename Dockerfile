FROM registry.access.redhat.com/ubi8/python-36
USER root
# Copying in source code
COPY --chown=1001:0 src /tmp/src/
COPY --chown=1001:0 requirements.txt /tmp/src/requirements.txt
COPY scripts /opt/app-root/bin
# this chmod is to assist the entrypoint
RUN chmod -R g=u /etc/passwd
USER 1001

# Assemble script sourced from builder image based on user input or image metadata.
# If this file does not exist in the image, the build will fail.
RUN /usr/libexec/s2i/assemble

# This script will inject the proper user information into the password file.
ENTRYPOINT [ "/opt/app-root/bin/uid_entrypoint" ]

# Run script sourced from builder image based on user input or image metadata.
# If this file does not exist in the image, the build will fail.
CMD /usr/libexec/s2i/run
