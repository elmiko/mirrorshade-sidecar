# Mirrorshade Sidecar

The mirrorshade sidecar is a container that will watch a directory for updates,
and then push any changes to an external store. For example, the initial storage
supported is a [Git](https://git-scm.org) repository, when new changes are
detected a commit is generated and pushed to the remote repository.

This project is primarily intended for usage with [Kubernetes](https://kubernetes.io)
and as such the documentation and examples are oriented towards those users.

## Getting Started

* [Using the default backend with a local container](docs/getting-started-local-container.md)

    The default backend will simply log all actions in the datadir to the console.

* [Using the Git backend with a local container](docs/getting-started-git-local-container.md)

    The Git backend will turn any changes in the datadir into Git commits and push them to
    a remote repository.
