# Getting Starting Using a Local Container

In this configuration mirrorshade-sidecar will run in a container on the host
system watching a local directory for changes, and printing out log messages
for every transaction in the directory.

## Prereqs

* a terminal shell of some sort
* access to the [`podman`](https://podman.io) or [`docker`](https://docker.com) command
  _this document uses podman, but they are interchangeable_

## Procedure

Now you will create a data directory to share with the mirrorshade-sidecar container,
and then start the. Once running, you can test it by creating a file in the local data directory.

1. *Create the data directory.*
   Make a directory in a temporary location such as `$HOME/tmp/mirrorshade-datadir`. The mirrorshade-sidecar
   container will watch this directory for interaction and log the changes it sees.
1. *Start the mirrorshade-sidecar container.*
   The following command will start the container, for this example it is being run from the root
   of the user's home with the directories and information specified above. You will need to
   change these values to fit your configuration.
   ```
   podman run --rm -it \
       -v "$(pwd)/tmp/mirrorshade-datadir":/datadir:Z \
       quay.io/elmiko/mirrorshade-sidecar
   ```
   This command will start the container attached to the local terminal and remove it when it has finished.

If everything has worked you will see an output similar to this:

![Asciinema of mirrorshade-sidecar](mirrorshade-default.gif)

_Note: this animation shows interaction with the directory that is happening in a separate terminal window_

