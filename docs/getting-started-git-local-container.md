# Getting Starting Using a Local Container

In this configuration mirrorshade-sidecar will run in a container on the host
system, watching a local directory for changes, and using a separate local
directory for configuration files.

## Prereqs

* a terminal shell of some sort
* access to the [`podman`](https://podman.io) or [`docker`](https://docker.com) command
  _this document uses podman, but they are interchangeable_
* access to a remote [Git](https://git-scm.org) account (such as on GitHub or GitLab) using
  an SSH private key file for access.

## Procedure

Now you will create a data directory to hold the remote git repository, then create the
Git configuration files, and lastly start the mirrorshade-sidecar container. Once running
you test it by creating a file in the local data directory.

1. *Create the data directory.*
   Make a directory in a temporary location such as `$HOME/tmp/mirrorshade-datadir`. Mirrorshade
   will clone the remote repository into this directory, and then watch it for changes.
   This directory needs to be empty.
   Ensure that the directory is writeable by the container user. You can either add write
   permission or change the container user when running.
1. *Create the configration directory.*
   Make a directory in a temporary location such as `$HOME/tmp/mirrorshade-datadir`.
1. *Create the backend file.*
   Create a file named `backend` in the configuration directory, it should contain the
   single world `git` as its contents. This will instruct mirrorshade-sidecar to use the
   Git backend.
1. *Create the Git URL file.*
   Create a file named `git.url` in the configuration directory, it should contain the
   URL of the remote repository, eg `git@gitlab.com:elmiko/mirrorshade-test-repo.git`.
   Make sure that the key you are using has appropriate privileges for the remote repository,
   and that you are using an SSH URL.
1. *Create the git SSH key file.*
   Create a file named `git.key` in the configuration directory, it should contain an SSH
   private for the remote repository you have configured in the Git URL file.
   The SSH key needs to be the private key, please be careful not to use a sensitive key for
   this. Additionally, the key needs to be readable by the container user.
1. *Start the mirrorshade-sidecar container.*
   The following command will start the container, for this example it is being run from the root
   of the user's home with the directories and information specified above. You will need to
   change these values to fit your configuration.
   ```
   podman run --rm -it \
       -v "$(pwd)/tmp/mirrorshade-datadir":/datadir:Z \
       -v "$(pwd)/tmp/mirrorshade-config":/etc/mirrorshade:Z \
       quay.io/elmiko/mirrorshade-sidecar
   ```
   This command will start the container attached to the local terminal and remove it when it has finished.

If everything has worked you will see an output similar to this:

![Asciinema of mirrorshade-sidecar start](mirrorshade-git-start.gif)

In a different terminal shell try creating a file in the local data directory. You should see an output
from the mirrorshade-sidecar container logging the commit and pushing to the remote repository. It
should look similar to this:

![Asccinema of mirrorshade-sidecar add file](mirrorshade-git-addfile.gif)

Looking at the git history of the repository will show the changes made by a user "mirrorshade-bot".
